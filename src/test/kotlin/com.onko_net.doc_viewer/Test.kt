package com.onko_net.doc_viewer

import com.onko_net.doc_viewer.service.Service
import com.onko_net.doc_viewer.utils.logger
import org.junit.*
import org.junit.Test
import org.junit.runners.MethodSorters
import java.io.File
import java.net.URLEncoder
import java.nio.file.Paths

@Ignore
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class Test {
    companion object {
        val log = logger()
//        val reader = MDReader()
        @BeforeClass
        fun before() {
//            Service.service
        }
    }

    @Test
    fun reg() {
        val regexGroup = Regex("группа:\\w+")
        println("группа:текст ".contains(regexGroup))
    }

    @Test
    fun kFile() {
        val base = File("D:/workspace SSD/doc-viewer/docs")
        val file = File("D:/workspace SSD/doc-viewer/docs/abc/child/file1.txt")
        println(base.resolve("111/222/3.txt").normalize())
        println(file.relativeTo(base).normalize())
    }

    @Test
    fun childPath() {
        val parent = File("D:/workspace SSD/doc-viewer/docs")
        val child1 = parent.resolve("c15")
        val child2 = parent.resolve("dir/c15")
        val childB1 = parent.resolve("../psw")
        val childB2 = parent.resolve("../../psw")
        Assert.assertTrue(Service.isChildPath(child1, parent))
        Assert.assertTrue(Service.isChildPath(child2, parent))
        Assert.assertFalse(Service.isChildPath(childB1, parent))
        Assert.assertFalse(Service.isChildPath(childB2, parent))
    }


    @Test
    fun encode() {
        println(URLEncoder.encode("file/file/file x", "UTF-8"))
        println(URLEncoder.encode("file\\file\\file x", "UTF-8"))
    }

}
