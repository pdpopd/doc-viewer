package com.onko_net.doc_viewer.views

import com.onko_net.doc_viewer.model.MDFile
import com.onko_net.doc_viewer.service.Service
import com.vaadin.flow.component.HtmlComponent
import com.vaadin.flow.component.html.Anchor
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.Label
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import java.io.File

class Card(mdFile: MDFile) : VerticalLayout() {

    init {
        width = "100%"
        style["background"] = "aliceblue"

        val innerHtml = if (mdFile.html.isNotBlank()) mdFile.html else "Без заглавия."

        mdFile.resources.ifEmpty { listOf(null) }.forEachIndexed { idx, file ->
            if (idx == 1)
                add(Label("Дополнительные сведения:"))

            add(createAnchor(file, if (idx == 0) innerHtml else "файл: ${idx + 1}"))
        }
    }

    private fun createAnchor(file: File?, text: String): HtmlComponent {
        val url = file?.let { Service.service.pathFromDocDirUrlEncoded(it) }
        val canRead = file?.let { it.isFile && it.canRead() } ?: false
        return Div(
                if (canRead)
                    Anchor("download?file=${url}", text).apply {
                        setTarget("_blank")
                    }
                else
                    Anchor().apply { this.text = text }
        ).apply {
            if (!canRead)
                this.add(HtmlComponent("sup").apply {
                    element.text = "Файл не найден."
                    className = "extension-sup"
                })
        }
    }
}
