package com.onko_net.doc_viewer.views

import com.onko_net.doc_viewer.model.Icd10
import com.onko_net.doc_viewer.utils.logger
import com.vaadin.flow.component.Key
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.component.treegrid.TreeGrid
import com.vaadin.flow.data.provider.hierarchy.TreeData
import com.vaadin.flow.data.provider.hierarchy.TreeDataProvider

class MkbTreeView(items: Collection<Icd10>) : TreeGrid<Icd10>() {
    val log = logger()
    val dataProvider = TreeDataProvider<Icd10>(
            TreeData<Icd10>()
                    .addItems(items) { x -> x.children }
    )

    init {
        setDataProvider(dataProvider)
        setSelectionMode(SelectionMode.SINGLE)

        addHierarchyColumn(Icd10::code).apply {
            this.setHeader("Код")
            this.width = "9rem"
            this.element.style["max-width"] = "12rem"
        }
        addColumn(Icd10::name).setHeader("Наименование").isAutoWidth = true
    }
}

class MkbTreeDialog(mkbTreeView: MkbTreeView, confirm: (Icd10) -> Unit)
    : Dialog() {
    val log = logger()
    private val tfFilter = TextField().apply {
        width = "30rem"
        maxWidth = "50vw"
        placeholder = "Поиск..."
    }
    private val confirmButton = Button("Поиск") { _ ->
        val selected = mkbTreeView.selectedItems
        if (selected.isNotEmpty()) {
            confirm(selected.first())
            close()
        }
    }.apply {
        this.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
        this.addClickShortcut(Key.ENTER)
        this.isEnabled = false
    }
    private val cancelButton = Button("Отмена") { _ ->
        close()
    }

    private fun filterMkb10(icd10: Icd10, s: String): Boolean {
        return icd10.code.contains(s) ||
                icd10.name.contains(s) ||
                icd10.children.any { x -> filterMkb10(x, s) }
    }

    init {
        width = "80vw"
        height = "80vh"

//        mkbTreeView.addItemDoubleClickListener {
//            confirm(it.item)
//            close()
//        }
        mkbTreeView.addSelectionListener { event ->
            confirmButton.isEnabled = event.firstSelectedItem.isPresent
        }

        tfFilter.addValueChangeListener { e ->
            if (e.value.isNotBlank())
                mkbTreeView.dataProvider.setFilter { t: Icd10 ->
                    filterMkb10(t, e.value)
                }
            else
                mkbTreeView.dataProvider.clearFilters()
        }

        val hlFilter = HorizontalLayout(tfFilter).apply {
            this.defaultVerticalComponentAlignment = FlexComponent.Alignment.CENTER
            this.justifyContentMode = FlexComponent.JustifyContentMode.CENTER
        }

        val hlButtons = HorizontalLayout(confirmButton, cancelButton).apply {
            this.defaultVerticalComponentAlignment = FlexComponent.Alignment.CENTER
            this.justifyContentMode = FlexComponent.JustifyContentMode.CENTER
        }

        val vl = VerticalLayout(hlFilter, mkbTreeView, hlButtons).apply {
            this.setFlexGrow(1.0, mkbTreeView)
            setSizeFull()
        }.apply {
            this.defaultHorizontalComponentAlignment = FlexComponent.Alignment.CENTER
        }
        add(vl)

    }
}
