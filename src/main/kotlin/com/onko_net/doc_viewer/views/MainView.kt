package com.onko_net.doc_viewer.views

import com.onko_net.doc_viewer.model.GroupItem
import com.onko_net.doc_viewer.model.MDFile
import com.onko_net.doc_viewer.model.Icd10
import com.onko_net.doc_viewer.service.Service
import com.onko_net.doc_viewer.utils.logger
import com.vaadin.flow.component.AttachEvent
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.applayout.AppLayout
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.Label
import com.vaadin.flow.component.listbox.ListBox
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.FlexLayout
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.page.Push
import com.vaadin.flow.component.splitlayout.SplitLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.renderer.ComponentRenderer
import com.vaadin.flow.router.*
import com.vaadin.flow.server.PWA
import elemental.json.Json
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

/**
 * The main view.
 */
@Route("")
@Push
@PWA(name = "OnkoNet Documentation Viewer", shortName = "DocViewer")
@CssImport("./styles/custom.css")
class MainView : AppLayout(), HasUrlParameter<String> {
    companion object {
        val log = logger()

        const val PARAM_SEARCH = "search"
        const val PARAM_GROUP = "group"
        const val ICD_PREFIX = "мкб:"
        val regexIcd10 = Regex("мкб:C[\\.\\d]+")
        val regexWhitespace = Regex("\\s+")
    }

    private val giAllItems = GroupItem("Все", 0, 0)
    var items = emptyList<MDFile>()
    var groupedItems = mapOf<String, List<MDFile>>().withDefault { emptyList() }
    var groups = listOf<GroupItem>()
    var currentLocationPath: String? = null
    var currentLocationParameters: MutableMap<String, List<String>>? = null

    private val groupName = Label().apply { isVisible = false; className = "bold" }
    private val hlGroupName = HorizontalLayout(groupName)
            .apply { this.justifyContentMode = FlexComponent.JustifyContentMode.CENTER; setWidthFull() }
    private val cardLayout = FlexLayout().apply {
        this.wrapMode = FlexLayout.WrapMode.WRAP
    }

    private val vlDrawer = ListBox<GroupItem>().apply {
        this.element.style["white-space"] = "nowrap"
        this.setRenderer(ComponentRenderer { gi ->
            Label("${gi.name} (${gi.found})").apply {
                if (gi != giAllItems)
                    element.style["padding-left"] = "1rem"
                className = "bold"
            }
        })

        this.addValueChangeListener { event ->
            val name = event.value?.name
            changeCurrentLocationParameters(PARAM_GROUP, name)
            refreshContent()
        }
    }

    private val btnSearch = Button("Поиск") { _ -> search(tfFilter.value) }.apply {
        addThemeVariants(ButtonVariant.LUMO_PRIMARY)
    }

    private val tfFilter = TextField().apply {
        placeholder = "Поиск..."
        addValueChangeListener { event -> search(event.value) }
        width = "30rem"
        maxWidth = "50vw"
        isClearButtonVisible = true
        this.element.style["padding"] = "1rem"
    }

    private val mkbTreeView = MkbTreeView(Service.service.listTreeIcd10)
    private val mkb10Dialog = MkbTreeDialog(mkbTreeView, this::eventSelectedMKB)
    private val someInfoContent = VerticalLayout(Div().apply {
        element.setProperty("innerHTML", Service.service.someInfoContent())
        maxWidth = "60rem"
    }).apply {
        this.defaultHorizontalComponentAlignment = FlexComponent.Alignment.CENTER
        setWidthFull()
    }

    private val btnShowMkb10Dialog = Button("МКБ10") { _ ->
        mkb10Dialog.open()
    }.apply {
        addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
    }

    private val divFilter = HorizontalLayout(tfFilter, btnSearch).apply {
        this.defaultVerticalComponentAlignment = FlexComponent.Alignment.CENTER
        this.justifyContentMode = FlexComponent.JustifyContentMode.CENTER
        setWidthFull()
    }

    init {
        tfFilter.suffixComponent = btnShowMkb10Dialog
        addToNavbar(divFilter)
        content = SplitLayout(vlDrawer, VerticalLayout(hlGroupName, cardLayout))
                .apply {
                    this.setSizeFull()
                    this.setSplitterPosition(30.0)
                }
    }

    fun eventSelectedMKB(icd: Icd10) {
        val s = tfFilter.value
        if (s.contains(regexIcd10))
            tfFilter.value = s.replace(regexIcd10, "$ICD_PREFIX${icd.code}").replace(regexWhitespace, " ").trim()
        else
            tfFilter.value = (s + " $ICD_PREFIX${icd.code}").replace(regexWhitespace, " ").trim()
    }

    @ExperimentalCoroutinesApi
    override fun onAttach(attachEvent: AttachEvent) {
        val ui = attachEvent.ui
        if (!Service.indexReadyStatus) {
            tfFilter.isEnabled = false
            Notification("Индексация документов. Ожидайте...", 3000,
                    Notification.Position.MIDDLE).open()

            GlobalScope.launch {
                Service.indexStatus.consumeEach { str ->
                    log.info("consume from channel: $str")
                    ui.access {
                        log.info("set isEnabled")
                        tfFilter.isEnabled = true
                        Notification("Индексация документов завершена.", 3000,
                                Notification.Position.MIDDLE).open()
                    }
                }
            }
        }

        search("")
    }

    fun changeCurrentLocationParameters(key: String, value: String?) {
        if (value.isNullOrBlank())
            currentLocationParameters?.remove(key)
        else
            currentLocationParameters?.set(key, mutableListOf(value))
        val nUrl = Location(currentLocationPath, QueryParameters(currentLocationParameters)).pathWithQueryParameters
        log.info(nUrl)
        UI.getCurrent().page.history.pushState(Json.createObject(), nUrl)
    }

    fun search(str: String?) {
        changeCurrentLocationParameters(PARAM_SEARCH, str)

        items = if (!str.isNullOrBlank())
            Service.service.searchFiles(str)
        else
            emptyList()

        giAllItems.found = items.size
        groupedItems = items.groupBy { it.title }.toSortedMap()
        groups = groupedItems.map { GroupItem(it.key, it.value.size, 0) }.let { list ->
            if (list.isNotEmpty())
                listOf(giAllItems) + list
            else
                list
        }
        vlDrawer.setItems(groups)
        if (groups.isNotEmpty())
            vlDrawer.value = giAllItems

        refreshContent()
    }

    fun refreshContent() {
        val sp = currentLocationParameters?.get(PARAM_SEARCH)?.firstOrNull()
        val gp = currentLocationParameters?.get(PARAM_GROUP)?.firstOrNull()
        cardLayout.removeAll()
        groupName.isVisible = false
        when {
            sp.isNullOrBlank() && gp.isNullOrBlank() -> cardLayout.add(someInfoContent)
            items.isEmpty() -> cardLayout.add(Label("Не найдено."))
            items.isNotEmpty() && gp == giAllItems.name -> {
                groupedItems.map { gis ->
                    VerticalLayout(Label(gis.key).apply { className = "bold" }, *(gis.value.map { md -> Card(md) }.toTypedArray())).apply {
                        this.defaultHorizontalComponentAlignment = FlexComponent.Alignment.CENTER
                        this.className = "child-flex"
                    }
                }.map { cardLayout.add(it) }
            }

            items.isNotEmpty() && !gp.isNullOrBlank() -> {
                groupName.isVisible = true
                groupName.text = gp
                groupedItems.getOrDefault(gp, emptyList())
                        .map { x -> Card(x).apply { className = "child-flex" } }
                        .forEach { c -> cardLayout.add(c) }

            }
            else -> {
            }
        }
    }

    override fun setParameter(event: BeforeEvent, @OptionalParameter parameter: String?) {
        log.debug("setParameter")
        currentLocationPath = event.location?.path
        currentLocationParameters = event.location?.queryParameters?.parameters?.toMutableMap()
        val parameters = (event.location?.queryParameters?.parameters ?: mapOf()).withDefault { _ -> listOf<String>() }
        event.ui.access {
            parameters[PARAM_SEARCH]?.first()?.let { str -> tfFilter.value = str }
            parameters[PARAM_GROUP]?.first()?.let { str -> vlDrawer.value = groups.find { it.name == str } }
        }
    }
}
