package com.onko_net.doc_viewer.lucene

import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.ru.RussianAnalyzer
import org.apache.lucene.document.Document
import org.apache.lucene.index.DirectoryReader
import org.apache.lucene.index.IndexReader
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.store.FSDirectory

import java.nio.file.Paths

class Indexer(val pathToIndexFolder: String) {

    fun index(create: Boolean, documents: List<Document>, analyzer: Analyzer) {
//        ByteBuffersDirectory in memory
        val dir = FSDirectory.open(Paths.get(pathToIndexFolder))
        val iwc = IndexWriterConfig(analyzer)
        if (create) {
            // Create a new index in the directory, removing any
            // previously indexed documents:
            iwc.openMode = IndexWriterConfig.OpenMode.CREATE
        } else {
            // Add new documents to an existing index:
            iwc.openMode = IndexWriterConfig.OpenMode.CREATE_OR_APPEND
        }

        val w = IndexWriter(dir, iwc)
        w.addDocuments(documents)
        w.close()
    }

    fun index(create: Boolean?, documents: List<Document>) {
        val analyzer = RussianAnalyzer()
        index(create!!, documents, analyzer)
    }

    fun readIndex(): IndexReader {
        val dir = FSDirectory.open(Paths.get(pathToIndexFolder))
        return DirectoryReader.open(dir)
    }
}
