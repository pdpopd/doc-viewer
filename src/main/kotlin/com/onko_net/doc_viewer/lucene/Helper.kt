package com.onko_net.doc_viewer.lucene

import com.onko_net.doc_viewer.model.MDFile
import com.onko_net.doc_viewer.utils.logger
import org.apache.lucene.document.Document
import org.apache.lucene.document.Field
import org.apache.lucene.document.FieldType
import org.apache.lucene.index.IndexOptions

object Helper {
    val log = logger()

    val textIndexedType: FieldType by lazy {
        val ft = FieldType()
        ft.setStored(true)
        ft.setIndexOptions(IndexOptions.DOCS)
        ft.setTokenized(true)
        ft
    }

    val textNotIndexedType: FieldType by lazy {
        val ft = FieldType()
        ft.setStored(true)
        ft.setIndexOptions(IndexOptions.NONE)
        ft.setTokenized(true)
        ft
    }

    fun mdToDocument(mdFile: MDFile, body: CharSequence): Document {
        val doc = Document()
        doc.add(Field("file_path", mdFile.file.canonicalPath, textNotIndexedType))
        doc.add(Field("uuid", mdFile.uuid.toString(), textNotIndexedType))
        doc.add(Field("мкб", mdFile.icd10Codes.joinToString(), textIndexedType))
        doc.add(Field("body", body, textIndexedType))
        doc.add(Field("группа", mdFile.title, textIndexedType))
        return doc
    }

}
