package com.onko_net.doc_viewer.lucene

import com.onko_net.doc_viewer.utils.logger
import org.apache.lucene.analysis.ru.RussianAnalyzer
import org.apache.lucene.index.IndexReader
import org.apache.lucene.index.Term
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search.FuzzyQuery
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.search.ScoreDoc
import org.apache.lucene.search.TermQuery
import java.util.*

class Searcher(val reader: IndexReader) {
    val log = logger()
    val DEFAULT_LIMIT = 100

    fun searchIndexWithTermQuery(toSearch: String, searchField: String, limit: Int): List<UUID> {
        val indexSearcher = IndexSearcher(reader)

        val term = Term(searchField, toSearch)
        val query = TermQuery(term)
        val search = indexSearcher.search(query, limit)
        val hits = search.scoreDocs
        showHits(hits)
        return extractUUID(hits)
    }

    fun customSearchByFields(icd: String) {
        for (i in 0 until reader.maxDoc()) {
            //            if (reader.isDeleted(i))
            //                continue;

            val doc = reader.document(i)
            val docId = doc.get("docId")

            // do something with docId here...
        }
    }

    fun searchIndexWithTermQueryByBody(toSearch: String): List<UUID> =
            searchIndexWithTermQuery(toSearch, "body", DEFAULT_LIMIT)

    fun searchInBody(toSearch: String, limit: Int): List<UUID> {
        log.info("search: $toSearch")
        val indexSearcher = IndexSearcher(reader)

        val queryParser = QueryParser("body", RussianAnalyzer())

        queryParser.defaultOperator = QueryParser.Operator.AND

        val query = queryParser.parse(toSearch)
        log.info("Type of query: " + query.javaClass.simpleName)
        log.info("query: ${query.toString()}")
        val search = indexSearcher.search(query, limit)
        val hits = search.scoreDocs
        showHits(hits)
        return extractUUID(hits)
    }

    fun searchInBody(toSearch: String): List<UUID> = searchInBody(toSearch, DEFAULT_LIMIT)

    fun fuzzySearch(toSearch: String, searchField: String, limit: Int): List<UUID> {
        val indexSearcher = IndexSearcher(reader)

        val term = Term(searchField, toSearch)

        val maxEdits = 2 // This is very important variable. It regulates fuzziness of the query
        val query = FuzzyQuery(term, maxEdits)
        val search = indexSearcher.search(query, limit)
        val hits = search.scoreDocs
        showHits(hits)
        return extractUUID(hits)
    }

    fun fuzzySearch(toSearch: String) = fuzzySearch(toSearch, "body", DEFAULT_LIMIT)

    private fun extractUUID(hits: Array<ScoreDoc>): List<UUID> =
            hits.map { hit -> reader.document(hit.doc).get("uuid").let { UUID.fromString(it) } }

    private fun showHits(hits: Array<ScoreDoc>) {
        if (hits.isEmpty()) {
            log.debug("\n\tНичего не найдено")
            return
        }
        log.debug("\n\tРезультаты поиска:")
        for (hit in hits) {
            val title = reader.document(hit.doc).get("title")
            log.debug("\n\tDocument Id = " + hit.doc + "\n\ttitle = " + title)
        }
    }
}
