package com.onko_net.doc_viewer.config

import com.onko_net.doc_viewer.utils.logger
import java.io.File

object Config {
    val log = logger()
    val docsDir: File get() = oncorDocs
    val icd10FileTsv: File get() = oncorDocs.resolve("icd10.tsv")
    val dirForIndex: File
        get() = File(System.getProperty("java.io.tmpdir")).resolve("OncorDocViewerIndex")
                .apply { log.info("dirForIndex: $this") }
    val someInfo: String get() = oncorDocs.resolve("info.html").canonicalPath

    private val oncorHome: File by lazy {
        log.info("ONCOR_HOME env: ${System.getenv("ONCOR_HOME")}")
        File((System.getenv("ONCOR_HOME") ?: "/oncor")).absoluteFile
    }

    private val oncorDocs: File by lazy { oncorHome.resolve("oncor-docs").apply { log.info("oncorDocs: $this") } }
}
