package com.onko_net.doc_viewer.servlets

import com.onko_net.doc_viewer.config.Config
import com.onko_net.doc_viewer.service.Service
import com.onko_net.doc_viewer.utils.logger
import java.io.File
import java.nio.file.Files
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet("/download", loadOnStartup = 1)
class DownloadServlet : HttpServlet() {
    companion object {
        val log = logger()
        private var path: String = "/download"
    }

    override fun init() {
        super.init()
        path = servletContext.contextPath.removeSuffix("/") + "/download"
        log.info(path)
    }

    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        val fileParameter = req.getParameter("file")
        log.info("file: $fileParameter")

        val fPath = Config.docsDir.resolve(fileParameter)

        if (Service.isChildPath(fPath, Config.docsDir) && fPath.canRead())
            fPath.okResponse(resp)
        else
            errorResponse("File can't read.", resp)
    }

    fun File.okResponse(response: HttpServletResponse): Unit {
        Files.probeContentType(toPath())?.let { ct ->
            response.contentType = ct
            if (ct.startsWith("text")) {
                response.characterEncoding = "UTF-8"
            }
        }
        response.setHeader("Content-disposition", "inline; filename=$name")
        response.setContentLengthLong(length())
        inputStream().use { it.copyTo(response.outputStream) }
    }

    fun errorResponse(text: String, response: HttpServletResponse): Unit {
        response.status = 404
//        val arr = text.toByteArray(Charsets.UTF_8)
//        response.outputStream.write(arr)
//        response.setContentLength("")
    }
}
