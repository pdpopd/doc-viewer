package com.onko_net.doc_viewer.service

import com.onko_net.doc_viewer.utils.logger
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener
import javax.servlet.annotation.WebListener

@WebListener
class AppServletContextListener : ServletContextListener {
    val log = logger()
    override fun contextInitialized(p0: ServletContextEvent?) {
        val t0 = System.currentTimeMillis()
        Service.service
        val t1 = System.currentTimeMillis()
        log.info("init service: ${t1 - t0} ms")
    }

    override fun contextDestroyed(p0: ServletContextEvent?) {

    }
}
