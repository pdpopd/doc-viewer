package com.onko_net.doc_viewer.service

import com.onko_net.doc_viewer.config.Config
import com.onko_net.doc_viewer.lucene.Helper
import com.onko_net.doc_viewer.lucene.Indexer
import com.onko_net.doc_viewer.lucene.Searcher
import com.onko_net.doc_viewer.model.GroupItem
import com.onko_net.doc_viewer.model.MDFile
import com.onko_net.doc_viewer.model.Icd10
import com.onko_net.doc_viewer.utils.logger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import org.apache.commons.io.FileUtils
import org.apache.lucene.index.IndexReader
import java.io.File
import java.net.URL
import java.net.URLEncoder
import java.util.*
import java.util.stream.Collectors

class Service {
    companion object {
        val log = logger()

        val service: Service by lazy { Service() }
        val indexStatus = Channel<String>()
        var indexReadyStatus = false

        fun isChildPath(child: File, parent: File) = child.normalize().startsWith(parent.normalize())

        inline fun <T> measureTimeMillis(block: () -> T): Pair<T, Long> {
            val start = System.currentTimeMillis()
            val result = block()
            return result to System.currentTimeMillis() - start
        }

        fun extractText(file: File): CharSequence = if (file.isFile && file.canRead())
            when (file.extension) {
                "html", "htm" -> MDReader.plainText(file)
                "pdf" -> PDFReader.extractText(file)
                else -> {
                    log.warn("Unknown file extension. ${file.canonicalPath}")
                    ""
                }
            }
        else {
            log.warn("Can't read file : ${file.canonicalPath}")
            ""
        }
    }

    val mapOfGroups = mutableMapOf<String, GroupItem>().withDefault { key -> GroupItem(key, 0, 0) }
    val hashPathToMDFile = mutableMapOf<UUID, MDFile>()
    var listTreeIcd10: List<Icd10> = ICD10TSVReader.parseTsv(Config.icd10FileTsv)
    val allIcd10Codes: Set<String> = ICD10TSVReader.availableIcd10(listTreeIcd10)
    val indexer = Indexer(Config.dirForIndex.canonicalPath)
    val reader: IndexReader by lazy { indexer.readIndex() }
    val searcher: Searcher by lazy { Searcher(reader) }

    init {
        log.info("Parsing .md files in ${Config.docsDir.canonicalPath}...")
        GlobalScope.launch {
            val (mdFiles, parseMDFilesTime) = measureTimeMillis {
                Config.docsDir.mdFiles()
                        .asSequence().toList()
                        .parallelStream()
                        .map { file ->
                            MDReader().parseMDFile(file, allIcd10Codes)
                        }.collect(Collectors.toList())
            }

            mdFiles.forEach { mdFile ->
                hashPathToMDFile[mdFile.uuid] = mdFile
            }

            mdFiles.map { it.title }.groupBy { it }.forEach { (key, list) -> mapOfGroups += key to GroupItem(key, 0, list.size) }

            log.info("Creating lucene index...")
            Config.dirForIndex.apply {
                mkdirs()
                FileUtils.cleanDirectory(this)
            }

            val (docs, mdToIndexDocTime) = measureTimeMillis {
                mdFiles.parallelStream().map { mdFile ->
                    Helper.mdToDocument(mdFile, mdFile.resources.joinTo(StringBuilder(), transform = { f -> Service.extractText(f) }))
                }.collect(Collectors.toList())
            }

            val indexCreateTime = measureTimeMillis { indexer.index(true, docs) }

            //remove unused icd10
            listTreeIcd10 = listTreeIcd10.map { icd10 ->
                val nc = icd10.children.filter { c ->
                    mdFiles.any { md ->
                        md.regex?.let { r -> c.code.contains(r) } ?: false
                    }
                }.toMutableList()

                icd10.copy(children = nc)
            }.filter { it.children.isNotEmpty() }

            log.info("Created lucene index.")
            log.info("""Durations of init: 
                |parseMDFilesTime: $parseMDFilesTime ms 
                |mdToIndexDocxTime: $mdToIndexDocTime ms
                |indexCreateTime: $indexCreateTime ms""")
            indexStatus.send("100")
            log.info("send to channel")
            indexReadyStatus = true
        }

    }

    fun searchFiles(text: String?): List<MDFile> {
        if (text.isNullOrBlank())
            return hashPathToMDFile.values.toList()
        return searchInIndex(text)
    }

    private fun searchInIndex(str: String): List<MDFile> = searcher.searchInBody(str)
            .mapNotNull { uuid -> hashPathToMDFile[uuid] }

    fun File.mdFiles() = FileUtils.iterateFiles(this, arrayOf("md"), true)

    fun pathFromDocDir(mdFile: MDFile): String = mdFile.resources.firstOrNull()?.let { pathFromDocDir(it) } ?: ""

    fun pathFromDocDir(file: File): String = file.relativeTo(Config.docsDir).toString().replace('\\', '/')

    fun pathFromDocDirUrlEncoded(file: File): String = URLEncoder.encode(pathFromDocDir(file).replace('\\', '/'), "UTF-8")

    fun someInfoContent(): String = Config.someInfo.let { path ->
        try {
            when {
                path.isNullOrBlank() || !File(path).exists() -> "Здесь может быть любая информация."
                path.startsWith("http") -> URL(path).readText()
                else -> File(path).reader().readText()
            }
        } catch (t: Throwable) {
            log.error("$path can't read")
            "Указанный файл не может быть прочитан."
        }
    }

}
