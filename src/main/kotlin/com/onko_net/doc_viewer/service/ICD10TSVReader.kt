package com.onko_net.doc_viewer.service

import com.onko_net.doc_viewer.model.Icd10
import com.onko_net.doc_viewer.utils.logger
import java.io.File

object ICD10TSVReader {
    val log = logger()

    fun availableIcd10(list: List<Icd10>): Set<String> = list.flatMap { extractCode(it) }.toSet()

    fun extractCode(icd10: Icd10): List<String> = listOf(icd10.code) + icd10.children.flatMap { extractCode(it) }

    fun parseTsv(file: File): List<Icd10> {
        check(file.isFile && file.canRead()) { "Can't read tsv file: ${file.canonicalPath}" }

        val list = mutableListOf<Icd10>()

        file.reader().useLines { lines ->
            lines.drop(1).forEach { line ->
                val (code, name, group) = line.split("\t")
                val level = when {
                    code.contains(".") -> 1
                    else -> 0
                }
                val x = Icd10(code, name, group, level, mutableListOf())
                when (level) {
                    0 -> list += x
                    1 -> list.last().children += x
                }
            }
        }
        log.debug("tsv parse complete: ${list.size}")
        return list
    }
}
