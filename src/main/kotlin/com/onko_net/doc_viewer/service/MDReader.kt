package com.onko_net.doc_viewer.service

import com.onko_net.doc_viewer.model.MDFile
import com.onko_net.doc_viewer.utils.logger
import com.vladsch.flexmark.ext.yaml.front.matter.AbstractYamlFrontMatterVisitor
import com.vladsch.flexmark.ext.yaml.front.matter.YamlFrontMatterExtension
import com.vladsch.flexmark.html.HtmlRenderer
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.ast.Document
import com.vladsch.flexmark.util.data.MutableDataSet
import org.jsoup.Jsoup
import java.io.File
import java.nio.file.Paths
import java.util.*

class MDReader {
    companion object {
        val log = logger()
        val replaceH = Regex("<h1>|<h2>|<h3>")
        val replaceHEnd = Regex("</h1>|</h2>|</h3>")
        val options = MutableDataSet()
                .set(Parser.EXTENSIONS, arrayListOf(YamlFrontMatterExtension.create()))

        fun plainText(html: String) = Jsoup.parse(html).text()
        fun plainText(f: File) = Jsoup.parse(f, "UTF-8").text()
    }

    val parser = Parser.builder(options).build()

    val renderer = HtmlRenderer.builder(options).build()

    fun html(doc: Document) = renderer.render(doc)
            .replace("</p>\n<p>", "<br>\n")
            .replace(replaceH, "<h4>")
            .replace(replaceHEnd, "</h4>")

    fun plainText(doc: Document) = plainText(html(doc))

    fun parseMDFile(file: File, allIcd10Codes: Set<String>): MDFile {
        val fr = file.reader()
        val visitor = AbstractYamlFrontMatterVisitor()
        val document = parser.parseReader(fr)
        fr.close()

        visitor.visit(document)
        val data = visitor.data
        val regex = data["mkb"]?.first()?.let { Regex(it) }
        val title = data["title"]?.first() ?: ""

        val nameWithoutExtension = file.name.removeSuffix(".${file.extension}")
        val fileDir = file.parentFile.canonicalPath

        val resources = data["resources"]
                ?.map { fn -> Paths.get(fileDir, fn.replace("\$name", nameWithoutExtension)).toFile() }
                ?: emptyList()

        val icd10Codes = allIcd10Codes.filter { code -> regex?.matches(code) ?: false }.toSet()

        return MDFile(UUID.randomUUID(), title, file, regex, resources,
                metadata = data,
                icd10Codes = icd10Codes,
                html = plainText(document)
        )
    }
}
