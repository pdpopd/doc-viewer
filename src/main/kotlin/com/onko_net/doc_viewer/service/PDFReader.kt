package com.onko_net.doc_viewer.service

import com.onko_net.doc_viewer.utils.logger
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import java.io.File

object PDFReader {
    val log = logger()

    fun extractText(file: File): String = PDDocument.load(file).use { pdd ->
        if (!pdd.currentAccessPermission.canExtractContent()) {
            log.warn("You do not have permission to extract text from: ${file.canonicalPath}")
            return ""
        }

        val stripper = PDFTextStripper().apply {
            sortByPosition = true
        }

        stripper.getText(pdd).trim()
    }
}
