package com.onko_net.doc_viewer.model

import java.io.File
import java.util.*

data class MDFile(val uuid: UUID,
                  val title: String,
                  val file: File,
                  val regex: Regex?,
                  val resources: List<File>,
                  val metadata: Map<String, List<String>>,
                  val icd10Codes: Set<String> = emptySet(),
                  val html: String = "")

data class Icd10(val code: String,
                 val name: String,
                 val group: String,
                 val level: Int,
                 val children: MutableList<Icd10>)

data class GroupItem(var name: String, var found: Int, val count: Int)
